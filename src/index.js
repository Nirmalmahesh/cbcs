import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route } from 'react-router-dom';
import {Context} from "./ContextEngine";
import './css/main.css';
import asyncComponent from "./AsyncComponent";
import project from './dashboard/api';


const AsyncApp = asyncComponent(()=>import('./App'));
const AsyncSignupPage = asyncComponent(()=>import('./signup/signup'));
const AsyncLogout = asyncComponent(()=>import('./signup/logout'));
const AsyncDashboard = asyncComponent(()=>import('./dashboard/dashboard'));
const AsyncStudentList = asyncComponent(()=>import('./dashboard/student_list/student-list'));
const AsyncTimings = asyncComponent(()=>import('./dashboard/timing/timing'));
const AsyncCombineCourses = asyncComponent(()=>import('./dashboard/combine/combine'));
const AsyncListGroup = asyncComponent(()=>import('./dashboard/student/list_groups'));
const AsyncCourseList = asyncComponent(()=>import('./dashboard/student/course_list'));
const AsyncAddStaff = asyncComponent(()=>import('./dashboard/staff_enroll/add_staff'));
const AsyncGroupCourses = asyncComponent(()=>import('./dashboard/group'));
const AsyncReport = asyncComponent(()=>import('./dashboard/report'));
const AsyncForgotPassword = asyncComponent(()=>import('./signup/forgot'));
const AsyncDepartmentDashboard = asyncComponent(()=>import('./dashboard/department'));
const AsyncDepReport = asyncComponent(()=>import('./dashboard/department/report'));
const AsyncDepStudentList = asyncComponent(()=>import('./dashboard/department/student_list/student-list'));
const AsyncDepartManager = asyncComponent(()=>import('./dashboard/department_manager'));
const AsyncChangePassword = asyncComponent(()=>import('./signup/change_pass'));
const AsyncSetSecurityQuest = asyncComponent(()=>import('./signup/set_security'));
const AsyncAbout = asyncComponent(()=>import('./about'));
const AsyncCourses = asyncComponent(()=>import('./dashboard/department/courses'));
const AsyncManageCourse = asyncComponent(()=>import('./dashboard/manageCourse'));
const AsyncManageSelection = asyncComponent(()=>import('./dashboard/ManageSelection/index'));
const AsyncCourseStatus = asyncComponent(()=>import('./dashboard/status/index'));
const AsyncReportDash = asyncComponent(()=>import('./dashboard/report_upgrade/dashboard'));


ReactDOM.render(
    <Context>
    <BrowserRouter >   
        <div>
            <Route component={AsyncApp}/>
            <div>
                <Route exact path={`${project}/`} component={AsyncSignupPage}/>
                <Route path={`${project}/logout`} component={AsyncLogout}/>
                <Route path={`${project}/change_password`} component={AsyncChangePassword}/>
                <Route path={`${project}/set_security`} component={AsyncSetSecurityQuest}/>
                <Route exact path={`${project}/dashboard`} component={AsyncDashboard}/>
                <Route exact path={`${project}/department_portal`} component={AsyncDepartmentDashboard}/>
                <Route path={`${project}/dashboard/students-list`} component={AsyncStudentList}/>
                <Route path={`${project}/department_portal/students-list`} component={AsyncDepStudentList}/>
                <Route path={`${project}/dashboard/timing`} component={AsyncTimings}/>
                <Route path={`${project}/dashboard/combine_courses`} component={AsyncCombineCourses}/>
                <Route path={`${project}/dashboard/group_courses`} component={AsyncGroupCourses}/>
                <Route exact path={`${project}/student_portal`} component={AsyncListGroup}/>
                <Route path={`${project}/student_portal/courses`} component={AsyncCourseList}/>
                <Route path={`${project}/dashboard/add_staff`} component={AsyncAddStaff}/>
                <Route path={`${project}/dashboard/report`} component={AsyncReport}/>
                <Route path={`${project}/dashboard/department`} component={AsyncDepartManager}/>
                <Route path={`${project}/department_portal/report`} component={AsyncDepReport}/>
                <Route exact path={`${project}/forgot`} component={AsyncForgotPassword}/>
                <Route path={`${project}/department_portal/courses`} component={AsyncCourses}/>
                <Route exact path={`${project}/about`} component={AsyncAbout}/>
                <Route exact path={`${project}/dashboard/manage_course`} component={AsyncManageCourse} />
                <Route exact path={`${project}/dashboard/manage_selection`} component={AsyncManageSelection} />
                <Route exact path={`${project}/dashboard/course_status`} component={AsyncCourseStatus}/>
                <Route exact path={`${project}/dashboard/report_upgrade`} component={AsyncReportDash} />
            </div>
        </div>
    
</BrowserRouter>
    </Context>, document.getElementById('root'));
registerServiceWorker();
