import React from "react";
import Table from "./table";
import Loader from "../loader/loader";
import project from "../api";
import { faPlus } from "@fortawesome/fontawesome-free-solid/index.es";
import AlertNotify from '../alert.js'
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
class StudentList extends React.Component {
  state = {
    data: "",
    depart: [],
    sem: [],
    sec: [],
    dep_id: "",
    semester: "",
    section: "",
    table: [],
    newStudent:{
      roll_number:"",
      stud_name:"",
      depid:"",
      sem:"",
      sec:"",
      degeree:"",
      program:"",
      batch:"",
      type:"",
      password:"",
      question:"",
      answer:""

  },
  errors:{
    title:"",
    message:"",
    type:"",
    icon:""
}
    
  };

  onSubmit = e => {
    e.preventDefault();
    this.myCall("nothing", "student");
    this.setState({ isLoading: true });
  };

  onChangeNewEntry = e =>{
      if(e.target.name === "rollnumber"){
          this.setState({newStudent:{...this.state.newStudent,roll_number:e.target.value}})
      }else if(e.target.name === "name"){
        this.setState({newStudent:{...this.state.newStudent,stud_name:e.target.value}})
      }else if(e.target.name === "department"){
        this.setState({newStudent:{...this.state.newStudent,depid:e.target.value}})
      }else if(e.target.name === "sem"){
        this.setState({newStudent:{...this.state.newStudent,sem:e.target.value}})
      }else if(e.target.name === "sec"){
        this.setState({newStudent:{...this.state.newStudent,sec:e.target.value}})
      }else if(e.target.name === "degeree"){
        this.setState({newStudent:{...this.state.newStudent,degeree:e.target.value}})
      }else if(e.target.name === "program"){
        this.setState({newStudent:{...this.state.newStudent,program:e.target.value}})
      }else if(e.target.name === "batch"){
        this.setState({newStudent:{...this.state.newStudent,batch:e.target.value}})
      }else if(e.target.name === "type"){
        this.setState({newStudent:{...this.state.newStudent,type:e.target.value}})
      }
  }

  onSubmitNewEntry = (e) =>{
    e.preventDefault();
    this.setState({ isLoading: true });
     fetch(`${project}/api/addStudent`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify(this.state.newStudent)
      }).then((responce)=>{
          if(responce.ok)
          {
            this.setState({ isLoading: false });
            this.setState({
              errors:{
                title:"Added",
                message:"User Added Succefully",
                type:"success",
              }
            })
          }else{
            this.setState({ isLoading: false });
            console.log("Something Went Wrong");
          }
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  onChange = e => {
    if (e.target.name === "depart") {
      this.setState({ dep_id: e.target.value });
      this.setState({newStudent:{...this.state.newStudent,depid:e.target.value}});
      this.myCall(e.target.value, "semester");
      this.setState({ isLoading: true });
    } else if (e.target.name === "sem") {
      this.setState({ semester: e.target.value });
      this.setState({newStudent:{...this.state.newStudent,sem:e.target.value}})
      this.myCall(e.target.value, "section");
      this.setState({ isLoading: true });
    } else if (e.target.name === "sec") {
      this.setState({ section: e.target.value });
      this.setState({newStudent:{...this.state.newStudent,sec:e.target.value}})
    }
  };

  async myCall(value = "", name) {
    if (name === "semester") {
      const response = await fetch(`${project}/api/${name}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({ dep_id: value })
      }).catch(err => {
        console.log(err.message);
      });

      try {
        const data = await response.json();
        this.setState({ sem: data });
        this.setState({ isLoading: false });
      } catch (e) {
        console.log("FAILED");
      }
    } else if (name === "section") {
      const response = await fetch(`${project}/api/${name}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({ depid: this.state.dep_id, sem: value })
      }).catch(err => {
        console.log(err.message);
      });

      try {
        const data = await response.json();
        this.setState({ sec: data });
        this.setState({ isLoading: false });
      } catch (e) {
        console.log("FAILED");
      }
    } else {
      const response = await fetch(`${project}/api/${name}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({
          depid: this.state.dep_id,
          sem: this.state.semester,
          sec: this.state.section
        })
      }).catch(err => {
        console.log(err.message);
      });

      try {
        const data = await response.json();
        this.setState({ table: data });
        this.setState({ isLoading: false });
      } catch (e) {
        console.log("FAILED");
      }
    }
  }

  async componentWillMount() {
    const response = await fetch(`${project}/api/department`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    }).catch(err => {
      console.log(err.message);
    });

    try {
      const data = await response.json();
      this.setState({ depart: data });
    } catch (e) {
      console.log("FAILED");
    }
  }

  render() {
    const { isLoading,errors } = this.state;
    return (
      
      <div>
        {isLoading && <Loader />}
        <div id={"padding1"} className={"container "}>
        
          <div
            className={
              "row card border border-top-0 border-bottom-0 border-light bg-light"
            }
          >
            <div id={"padding-all"} className={"col-12"}>
              <h4 className={"card-body display-4"} align="center">
                Student List
                <hr />
              </h4>
              <form onSubmit={this.onSubmit}>
                <div className={"row container"}>
                  <div id={"department"} className="input-group mb-3 col-3">
                    <select
                      className="custom-select"
                      name="depart"
                      onChange={this.onChange}
                    >
                      <option value="">Department</option>
                      {this.state.depart.map(dap => {
                        return (
                          <option value={dap.dep_id}>{dap.dep_name}</option>
                        );
                      })}
                    </select>
                  </div>
                  <div id={"semester"} className="input-group mb-3 col-3">
                    <select
                      className="custom-select"
                      name="sem"
                      onChange={this.onChange}
                    >
                      <option value="">Semester</option>
                      {this.state.sem.map(dap => {
                        return <option value={dap.sem}>{dap.sem}</option>;
                      })}
                    </select>
                  </div>
                  <div
                    id={"section"}
                    className="input-group mb-3 col-3"
                    onChange={this.onChange}
                  >
                    <select className="custom-select" name="sec">
                      <option value="">Section</option>
                      {this.state.sec.length != 0 &&
                        this.state.sec != "" &&
                        this.state.sec.map(dap => {
                          return <option value={dap.sec}>{dap.sec}</option>;
                        })}
                    </select>
                  </div>
                  <div className={"col-3"}>
                    <button
                      type="submit"
                      className="btn btn-success btn-block "
                    >
                      OK
                    </button>
                  </div>
                </div>
              </form>
              {this.state.table && (
                <Table list={this.state.table} key={this.state.table.uid} />
              )}
            </div>
            <div id={"padding-all"} className={"col-12"}>
              <h4 className={"card-body display-4"} align="center">
                <FontAwesomeIcon icon={faPlus} />
                Add Student
                <hr />
              </h4>

              <form onSubmit={this.onSubmitNewEntry}>
              {errors.title !== "" && <AlertNotify icon={errors.icon} title={errors.title} type={errors.type} message={errors.message} />}
                <div className="form-row">
                  <div className="form-group col-md-6">
                    <label for="RollNumber">RollNumber</label>
                    <input
                      type="text"
                      className="form-control"
                      id="RollNumber"
                      placeholder="Roll Number"
                      name={"rollnumber"}
                      onChange={this.onChangeNewEntry}
                    />
                  </div>
                  <div className="form-group col-md-6">
                    <label for="Name">Name</label>
                    <input
                      type="text"
                      className="form-control"
                      name={"name"}
                      placeholder="Enter Your Name"
                      onChange={this.onChangeNewEntry}
                    />
                  </div>
                </div>
                <div className={"form-row"}>
                  <div className="form-group col-md-6">
                    <label>Department</label>
                    <select
                      className="custom-select"
                      name="depart"
                      onChange={this.onChange}
                      
                    >
                      <option value="">Department</option>
                      {this.state.depart.map(dap => {
                        return (
                          <option value={dap.dep_id}>{dap.dep_name}</option>
                        );
                      })}
                    </select>
                  </div>

                  <div className={"form-group col-md-3"}>
                    <label>Sem</label>
                    <select
                      className="custom-select"
                      name="sem"
                      onChange={this.onChange}
                      
                    >
                      <option value="">Semester</option>
                      {this.state.sem.map(dap => {
                        return <option value={dap.sem}>{dap.sem}</option>;
                      })}
                    </select>
                  </div>

                  <div className={"form-group col-md-3"}>
                    <label>Sec</label>
                    <select className="custom-select" name="sec" onChange={this.onChangeNewEntry}>
                      <option value="">Section</option>
                      {this.state.sec.length != 0 &&
                        this.state.sec != "" &&
                        this.state.sec.map(dap => {
                          return <option value={dap.sec}>{dap.sec}</option>;
                        })}
                    </select>
                  </div>
                </div>

                <div className={"form-row"}>
                  <div className={"form-group col-md-3"}>
                    <label>Degeree</label>
                    <input
                      type={"text"}
                      className={"form-control"}
                      placeholder={"Eg: BE or BSc or MCA"}
                      name={"degeree"}
                      onChange={this.onChangeNewEntry}
                    />
                  </div>
                  <div className={"form-group col-md-3"}>
                    <label>Program</label>
                    <input
                      type={"text"}
                      className={"form-control"}
                      name={"program"}
                      placeholder={"Eg: Computer Science and Engineering"}
                      onChange={this.onChangeNewEntry}
                    />
                  </div>
                  <div className={"form-group col-md-3"}>
                    <label>Batch</label>
                    <input
                      type={"text"}
                      className={"form-control"}
                      placeholder={"Eg: 2016-2020"}
                      name={"batch"}
                      onChange={this.onChangeNewEntry}
                    />
                  </div>
                  <div className={"form-group col-md-3"}>
                    <label>Type</label>
                    <input
                      type={"text"}
                      className={"form-control"}
                      placeholder={"Eg: UG or PG"}
                      name={"type"}
                      onChange={this.onChangeNewEntry}
                    />
                  </div>
                </div>

                <button type="submit" className="btn btn-primary">
                 ADD
                </button>
              </form>
            </div>
          </div>
          <br />
        </div>
      </div>
    );
  }
}

export default StudentList;
