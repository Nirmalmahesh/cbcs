import React from "react";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {
  faExclamationTriangle,
  faFileAlt,
  faGraduationCap,
  faPlus
} from "@fortawesome/fontawesome-free-solid/index.es";
import Loader from "../loader/loader";
import AlertNotify from "../alert";
import CombineTable from "./table_timing";
import TableComb from "./table_combine";
import project from "../api";

class GroupCourses extends React.Component {
  state = {
    depart: [],
    groupings: [],
    sem: [],
    dep_id: "",
    dep_id2: "",
    semester: "",
    elect: "",
    elect_name: "",
    course_list: [],
    comb_name: "",
    department: "",
    errors: {
      title: "",
      message: "",
      type: "",
      icon: ""
    },
    data: {
      start_date: "",
      end_date: ""
    },
    Course_data: {
      course_id: "",
      course_name: "",
      min_lim: "",
      min_lim_other: "",
      max_lim: "",
      max_lim_other: ""
    },
    Course_data_new: {
      course_id: "",
      course_name: "",
      min_lim: "",
      min_lim_other: "",
      max_lim: "",
      max_lim_other: "",
      prereq:"",
      sem:"",
      elect:"",
      dep_id:"",
      course_code:""
    }
  };
  constructor() {
    super();
    this.onSubmit3 = this.onSubmit3.bind(this);
  }
  onSubmit = e => {
    e.preventDefault();
    this.myCall("nothing", "combine_list");
  };

   onSubmit4 = e =>{
      e.preventDefault();

      
          const reponse =  fetch(`${project}/api/updateCourseData`,{
              headers: {
                  "Content-Type": "application/json",
                  Accept: "application/json"
                },  
                method: "post",
                body:JSON.stringify(this.state.Course_data)
          }).catch(err=>{
              console.log(err.message);
          });
  }
  onSubmit5 = async (e) =>{
    e.preventDefault();

      this.setState({ isLoading: false});
      this.setState({ isLoading: true });
      console.log("Inserting Data");
        const response =  await fetch(`${project}/api/InsertCourseData`,{
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
              },  
              method: "post",
              body:JSON.stringify(this.state.Course_data_new)
        }).catch(err=>{
            this.setState({
                errors: {
                    title: "Error",
                    message: "Something Went Wrong!!",
                    type: "danger",
                    icon: faExclamationTriangle
                }
            });
        });

        if(response.status != 200)
        {
            this.setState({
                errors: {
                    title: "Error",
                    message: "Something Went Wrong!!",
                    type: "danger",
                    icon: faExclamationTriangle
                }
            });
        }else{
            this.setState({
                errors: {
                    title: "Success",
                    message: "New Record Inserted Successfully",
                    type: "success",
                    icon: faExclamationTriangle
                }
            });
        }
      this.setState({ isLoading: false });
}

  onSubmit3 = e => {
    e.preventDefault();
    if (Array.from(this.selectedCheckboxes1).length !== 0) {
      this.myCall("getData");
    } else {
      this.setState({
        errors: {
          title: "Insert Value",
          message: "Value is Missing !!",
          type: "danger",
          icon: faExclamationTriangle
        }
      });
    }
  };
  onSubmit2 = e => {
    e.preventDefault();
    if (e.target.name === "update") {
      this.myCall2(e.target.name);
    } else if (e.target.name === "delete") {
      this.myCall2(e.target.name);
    } else if (e.target.name === "ok") {
      this.myCall2(e.target.name);
    }
  };

  async myCall2(name) {
    if (name === "delete") {
      this.setState({ isLoading: true });
      const response = await fetch(`${project}/api/${name}_grp`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({
          grp_names: Array.from(this.selectedCheckboxes2),
          dep_id: this.state.department
        })
      }).catch(err => {
        console.log(err.message);
      });

      try {
        await response.json();
        this.setState({ isLoading: false });
        window.location.reload();
      } catch (e) {
        this.setState({ groupings: [] });
        this.setState({ isLoading: false });
        console.log("FAILED");
      }
    } else if (name === "ok") {
      this.setState({ isLoading: true });
      const response = await fetch(`${project}/api/groupingsOfDep`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({ dep_id: this.state.department })
      }).catch(err => {
        console.log(err.message);
      });

      try {
        const data = await response.json();
        localStorage.setItem("groupings", JSON.stringify(data));
        this.setState({
          groupings: JSON.parse(localStorage.getItem("groupings"))
        });
        this.setState({ isLoading: false });
      } catch (e) {
        this.setState({ isLoading: false });
        console.log("FAILED");
      }
    }
  }


  onChangeData = e =>{

      if(e.target.name === "course_code"){
        this.setState({Course_data : {...this.state.Course_data,course_code:e.target.value}})
      }else if(e.target.name === "course_name"){
        this.setState({Course_data : {...this.state.Course_data,course_name:e.target.value}})
      }else if(e.target.name === "min_lim_own"){
        this.setState({Course_data : {...this.state.Course_data,min_lim:e.target.value}})
      }else if(e.target.name === "max_lim_own"){
        this.setState({Course_data : {...this.state.Course_data,max_lim:e.target.value}})
      }else if(e.target.name === "min_lim_other"){
        this.setState({Course_data : {...this.state.Course_data,min_lim_other:e.target.value}})
      }else if(e.target.name === "max_lim_other"){
        this.setState({Course_data : {...this.state.Course_data,max_lim_other:e.target.value}})
      }else if(e.target.name === "credit"){
        this.setState({Course_data : {...this.state.Course_data,credit:e.target.value}})
      }

  }

  
  onChangeDataNew = e =>{

    if(e.target.name === "course_code"){
      console.log("Course Code:"+this.state.Course_data_new.course_code);
      this.setState({Course_data_new : {...this.state.Course_data_new,course_code:e.target.value}})
    }else if(e.target.name === "course_name"){
      this.setState({Course_data_new : {...this.state.Course_data_new,course_name:e.target.value}})
    }else if(e.target.name === "min_lim_own"){
      this.setState({Course_data_new : {...this.state.Course_data_new,min_lim:e.target.value}})
    }else if(e.target.name === "max_lim_own"){
      this.setState({Course_data_new : {...this.state.Course_data_new,max_lim:e.target.value}})
    }else if(e.target.name === "min_lim_other"){
      this.setState({Course_data_new : {...this.state.Course_data_new,min_lim_other:e.target.value}})
    }else if(e.target.name === "max_lim_other"){
      this.setState({Course_data_new : {...this.state.Course_data_new,max_lim_other:e.target.value}})
    }else if(e.target.name === "credit"){
      this.setState({Course_data_new : {...this.state.Course_data_new,credit:e.target.value}})
    }else if(e.target.name === "sem")
    {
      this.setState({Course_data_new : {...this.state.Course_data_new,sem:parseInt(e.target.value)}})
    }else if(e.target.name === "prereq")
    {      
      this.setState({Course_data_new : {...this.state.Course_data_new,prereq:e.target.value}})
    }else if(e.target.name === "elect_id")
    {
      this.setState({Course_data_new : {...this.state.Course_data_new,elect:parseInt(e.target.value)}})
    }else if(e.target.name === "department")
    {
      this.setState({Course_data_new : {...this.state.Course_data_new,dep_id:parseInt(e.target.value)}})
    }
}


  onChange = e => {
    this.setState({
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });
    if (e.target.name === "depart") {
      this.setState({ dep_id: e.target.value });
      this.myCall(e.target.value, "semester");
    } else if (e.target.name === "depart2") {
      this.setState({ dep_id2: e.target.value });
    } else if (e.target.name === "sem") {
      this.setState({ semester: e.target.value });
    } else if (e.target.name === "elect") {
      this.setState({ elect: e.target.value });
      if (e.target.value === "1") {
        this.setState({ elect_name: "Professional Elective" });
      } else {
        this.setState({ elect_name: "Open Elective" });
      }
    } else if (e.target.name === "comb_name") {
      this.setState({ comb_name: e.target.value });
    } else if (e.target.name === "department") {
      this.setState({ department: e.target.value });
    }
  };
  onChange2 = e => {
    if (e.target.name === "check1[]") {
      if (this.selectedCheckboxes1.has(e.target.value)) {
        this.selectedCheckboxes1.delete(e.target.value);
      } else {
        this.selectedCheckboxes1.add(e.target.value);
      }
    } else if (e.target.name === "check2[]") {
      if (this.selectedCheckboxes2.has(e.target.value)) {
        this.selectedCheckboxes2.delete(e.target.value);
      } else {
        this.selectedCheckboxes2.add(e.target.value);
      }
    }
  };

  async myCall(value = "", name) {
    this.setState({ isLoading: true });


   

    if (name === "semester") {
      const response = await fetch(`${project}/api/${name}`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({ dep_id: value })
      }).catch(err => {
        console.log(err.message);
      });

      try {
        const data = await response.json();
        this.setState({ sem: data });
        this.setState({ isLoading: false });
      } catch (e) {
        this.setState({ isLoading: false });
        console.log("FAILED");
      }
    } else if (name === "combine_list") {
      const response = await fetch(`${project}/api/courses_list`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({
          dep_id: this.state.dep_id,
          sem: this.state.semester,
          elect: this.state.elect
        })
      }).catch(err => {
        console.log(err.message);
      });

      try {
        const data = await response.json();
        this.setState({ course_list: data });
        this.setState({ isLoading: false });
      } catch (e) {
        this.setState({ isLoading: false });
        console.log("FAILED");
      }
    } else {
      const id = Array.from(this.selectedCheckboxes1)[0];

      const responce = await fetch(`${project}/api/get_data_course`, {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json"
        },
        method: "post",
        body: JSON.stringify({
          course_id: id
        })
      }).catch(err => {
        console.log(err.message);
      });
      try {
        const data = await responce.json();
        
        this.setState({ Course_data: data[0] });
        console.log(data);
        this.setState({ isLoading: false });
      } catch (e) {
        this.setState({ isLoading: false });
        console.log("FAILED");
      }
    }
  }
  async componentWillMount() {
    this.setState({ isLoading: true });
    const response2 = await fetch(`${project}/api/department`, {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      }
    }).catch(err => {
      console.log(err.message);
    });

    try {
      const data = await response2.json();
      this.setState({ depart: data });
      this.setState({ isLoading: false });
    } catch (e) {
      this.setState({ isLoading: false });
      console.log("FAILED");
    }

    this.selectedCheckboxes1 = new Set();
    this.selectedCheckboxes2 = new Set();
  }

  render() {
    const { isLoading, errors } = this.state;
    return (
      <div>
        {isLoading && <Loader />}
        <div id={"padding1"} className={"container "}>
          <div
            className={
              "row card border border-top-0 border-bottom-0 border-light bg-light"
            }
          >
            <div id={"padding-all"} className={"col-12"}>
              <h4 className={"card-body display-4"} align="center">
                {" "}
                <FontAwesomeIcon icon={faGraduationCap} /> Manage Course
              </h4>
              <div id={"padding-all"}>
                <div id={"padding-all"} className={"card "}>
                  <form onSubmit={this.onSubmit}>
                    <div
                      id={"padding-all"}
                      className={" row justify-content-md-center container"}
                    >
                      <div className={"col-3"}>
                        <div id={"department"} className="input-group mb-3">
                          <select
                            className="custom-select"
                            name="depart"
                            onChange={this.onChange}
                          >
                            <option value="">Department</option>
                            {this.state.depart.map(dap => {
                              return (
                                <option key={dap.dep_id} value={dap.dep_id}>
                                  {dap.dep_name}
                                </option>
                              );
                            })}
                          </select>
                        </div>
                      </div>
                      <div className={"col-3"}>
                        <div id={"semester"} className="input-group mb-3">
                          <select
                            className="custom-select"
                            name="sem"
                            onChange={this.onChange}
                          >
                            <option value="">Sem</option>
                            {this.state.sem.map(dap => {
                              return (
                                <option key={dap.sem} value={dap.sem}>
                                  {dap.sem}
                                </option>
                              );
                            })}
                          </select>
                        </div>
                      </div>
                      <div className={"col-3"}>
                        <div id={"semester"} className="input-group mb-3">
                          <select
                            className="custom-select"
                            name="elect"
                            onChange={this.onChange}
                          >
                            <option value="">Elective Type</option>
                            <option value="1">Professional Elective</option>
                            <option value="2">Open Elective</option>
                          </select>
                        </div>
                      </div>

                      <div className={"col-1 mb-3"}>
                        <button type="submit" className={"btn btn-success"}>
                          OK
                        </button>
                      </div>
                    </div>
                  </form>
                  <h4 id={"padding-all"} className="card-title text-center">
                    Select courses and Department : {this.state.elect_name}
                  </h4>
                  {errors.title !== "" && (
                    <AlertNotify
                      icon={errors.icon}
                      title={errors.title}
                      type={errors.type}
                      message={errors.message}
                    />
                  )}
                  <form onSubmit={this.onSubmit3}>
                    <div className={"row justify-content-md-center"}>
                      <div className={"col-12"} align="center">
                        <CombineTable
                          table_data={this.state.course_list}
                          onUserInputChange={this.onChange2}
                          key={this.state.course_list.course_id}
                        />
                      </div>
                      <div className={"col-6"} align="center">
                        <button type="submit" className={"btn btn-success"}>
                          Edit
                        </button>
                      </div>
                    </div>
                    <hr />
                  </form>

                  <div className={"row "}>
                    <div className={"col-12"}>
                      <form onSubmit={this.onSubmit4}>
                        <div className="form-row">
                          <div className="form-group col-md-6">
                            <label for="CourseCode">CourseCode</label>
                            <input
                              type="text"
                              className="form-control"
                              id="course_code"                              
                              name="course_code"
                              value={this.state.Course_data.course_code}
                              onChange={(e)=>this.onChangeData(e)}
                            />
                          </div>
                          <div className="form-group col-md-6">
                            <label for="CourseName">CourseName</label>
                            <input
                              type="text"
                              className="form-control"
                              id="CourseName"
                              name="course_name"
                              placeholder="CourseName"
                              value={this.state.Course_data.course_name}
                              onChange={(e)=>this.onChangeData(e)}
                            />
                          </div>
                        </div>

                        <div className="form-row">
                          <div className="form-group col-md-2">
                            <label for="inputState">Min_lim_own</label>
                            <input
                              type="number"
                              className="form-control"
                              name="min_lim_own"
                              value={this.state.Course_data.min_lim}
                              onChange={(e)=>this.onChangeData(e)}
                            />
                          </div>
                          <div className="form-group col-md-2">
                            <label for="inputState">Max_lim_own</label>
                            <input
                              type="number"
                              className="form-control"
                              name="max_lim_own"
                              value={this.state.Course_data.max_lim}
                              onChange={(e)=>this.onChangeData(e)}
                            />
                          </div>
                          <div className="form-group col-md-2">
                            <label for="inputState">Min_lim_Other</label>
                            <input
                              type="number"
                              className="form-control"
                              name="min_lim_other"
                              value={this.state.Course_data.min_lim_other}
                              onChange={(e)=>this.onChangeData(e)}
                            />
                          </div>
                          <div className="form-group col-md-2">
                          <label for="inputState">Max_lim_Other</label>
                          <input
                            type="number"
                            className="form-control"
                            name="max_lim_other"
                            value={this.state.Course_data.max_lim_other}
                            onChange={(e)=>this.onChangeData(e)}
                          />
                        </div>
                        <div className="form-group col-md-2">
                          <label for="inputState">Credit</label>
                          <input
                            type="number"
                            className="form-control"
                            name="credit"
                            value={this.state.Course_data.credit}
                            onChange={(e)=>this.onChangeData(e)}
                          />
                        </div>
                        </div>
                        
                        <button type="submit" className="btn btn-primary">
                          Update
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
                <hr/>
              <div className={"row"}>
              
            <div className={"col-12"}>
            <h4 className={"display-4"} align="center">
              {" "}
              <FontAwesomeIcon icon={faPlus} /> Add Course
            </h4>
            <form onSubmit={this.onSubmit5}>
              <div className="form-row">
                <div className="form-group col-md-2">
                  <label for="CourseCode">CourseCode</label>
                  <input
                    type="text"
                    className="form-control"
                    id="course_code"                              
                    name="course_code"                    
                    onChange={(e)=>this.onChangeDataNew(e)}
                  />
                </div>
                <div className="form-group col-md-4">
                  <label for="CourseCode">Department</label>
                  <select
                  className="custom-select"
                  name="department"
                  onChange={this.onChangeDataNew}
                >
                  <option value="">Department</option>
                  {this.state.depart.map(dap => {
                    return (
                      <option key={dap.dep_id} value={dap.dep_id}>
                        {dap.dep_name}
                      </option>
                    );
                  })}
                </select>
                </div>
                <div className="form-group col-md-6">
                  <label for="CourseName">CourseName</label>
                  <input
                    type="text"
                    className="form-control"
                    id="CourseName"
                    name="course_name"
                    placeholder="CourseName"                    
                    onChange={(e)=>this.onChangeDataNew(e)}
                  />
                </div>
              </div>

              <div className="form-row">
                <div className="form-group col-md-2">
                  <label for="inputState">Min_lim_own</label>
                  <input
                    type="number"
                    className="form-control"
                    name="min_lim_own"                    
                    onChange={(e)=>this.onChangeDataNew(e)}
                  />
                </div>
                <div className="form-group col-md-2">
                  <label for="inputState">Max_lim_own</label>
                  <input
                    type="number"
                    className="form-control"
                    name="max_lim_own"                    
                    onChange={(e)=>this.onChangeDataNew(e)}
                  />
                </div>
                <div className="form-group col-md-2">
                  <label for="inputState">Min_lim_Other</label>
                  <input
                    type="number"
                    className="form-control"
                    name="min_lim_other"                    
                    onChange={(e)=>this.onChangeDataNew(e)}
                  />
                </div>
                <div className="form-group col-md-2">
                <label for="inputState">Max_lim_Other</label>
                <input
                  type="number"
                  className="form-control"
                  name="max_lim_other"                  
                  onChange={(e)=>this.onChangeDataNew(e)}
                />
              </div>
              <div className="form-group col-md-2">
                <label for="inputState">Credit</label>
                <input
                  type="number"
                  className="form-control"
                  name="credit"                  
                  onChange={(e)=>this.onChangeDataNew(e)}
                />
              </div>
              <div className="form-group col-md-2">
                <label for="inputState">Sem</label>
                <input
                  type="number"
                  className="form-control"
                  name="sem"                  
                  onChange={(e)=>this.onChangeDataNew(e)}
                />
              </div>
              <div className="form-group col-md-6">
                <label for="inputState">Pre Requirment  </label>
                <input
                  type="text"
                  className="form-control"
                  name="prereq"            
                  onChange={(e)=>this.onChangeDataNew(e)}
                />
              </div>
              <div className="form-group col-md-6">
                <label for="inputState">Elective Type</label>
                <select
                            className="custom-select"
                            name="elect_id"
                            onChange={(e)=>this.onChangeDataNew(e)}
                          >
                            <option value="">Elective Type</option>
                            <option value="1">Professional Elective</option>
                            <option value="2">Open Elective</option>
                          </select>
              </div>
              </div>
              
              <button type="submit" className="btn btn-primary">
                Add
              </button>
            </form>
          </div>
              </div>
              </div>
            </div>
          </div>
          <br />
        </div>
      </div>
    );
  }
}

export default GroupCourses;
