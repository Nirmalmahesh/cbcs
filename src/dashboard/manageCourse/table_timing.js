import React from 'react';
import "../../css/main.css"

class CombineTable extends React.Component {


    render(){

        return(
            <div  className={"container"} >
                <div id={"overflow2"}>
                <table id="MyTable" className="table table-hover table-bordered" cellSpacing="0" width="100%">
                    <thead className={"thead-light"}>
                    <tr>
                        <th>Check</th>
                        <th>Course Code</th>
                        <th>Course Name</th>
                        <th>Min Limit own</th>
                        <th>Max Limit own</th>
                        <th>Min Limit Other</th>
                        <th>Max Limit Others</th>
                        <th>Credit</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.table_data.map((data =>{
                        return  (
                            <tr>
                                <td><input className="input-group" value={data.course_id} name="check1[]" onChange={this.props.onUserInputChange} type="checkbox" /></td>
                                <td>{data.course_code}</td>
                                <td>{data.course_name}</td>
                                <td>{data.min_lim}</td>
                                <td>{data.max_lim}</td>
                                <td>{data.min_lim_other}</td>
                                <td>{data.max_lim_other}</td>
                                <td>{data.credit}</td>
                            </tr>)
                    }))}
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

export default CombineTable;