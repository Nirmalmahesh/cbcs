import React from 'react';
import EXCEL from './excelConfig'
import COLVIS from './colVis'
import PDF from './pdfConfig'
import {Consumer} from "../../ContextEngine";
import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
import 'datatables.net-bs4/js/dataTables.bootstrap4'
import 'datatables.net-buttons-bs4'
import 'datatables.net-buttons-bs4/js/buttons.bootstrap4'
import 'datatables.net/js/jquery.dataTables'

var $  = require( 'jquery' );
 
 $.Datatable =require('datatables.net-bs4');
$.Datatable=require( 'datatables.net-buttons/js/buttons.html5.js' );
$.Datatable=require('datatables.net-buttons/js/buttons.colVis');

 
class Table extends React.Component{
    state = {
        report : []
    }
    dataTable = null;
    async componentWillUnmount()
    {
        this.dataTable.destroy();
    }
    async componentDidMount()
    {
       console.log(this.el);
       this.$el = $(this.el);
       var dataTable =await this.$el.DataTable(
        {
            dom: 'Bfrtip',
            "columnDefs": [ 
                {
                    targets:[6,7,8,9,10],
                    visible:false
                },
                {
                "searchable": true,
                "orderable": true,
                "targets": 0
                } ],
            "order": [[ 1, 'asc' ]],
            buttons: [ 
                EXCEL,
                PDF,
                COLVIS,
                
             ]
            
        });
        await dataTable.on( 'order.dt search.dt', function () {
            dataTable.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
                dataTable.cell(cell).invalidate('dom');
            } );
        } ).draw(); 
        this.dataTable = dataTable;
    }
    render()
    {
        return(
            <Consumer>
               
                {(consumer)=>{
                    return(
                    <React.Fragment>
                        
                            <table className={"table table-striped table-hover table-responsive"} id="report" ref={el=>this.el = el}>
                            <thead>
                            <tr>
                                <th scope="col">S. No.</th>
                                <th scope="col">Batch</th>
                                <th scope="col">Regulation</th>
                                <th scope="col">Program</th>
                                <th scope="col">Course Code</th>
                                <th scope="col">Course Name</th> 
                                <th scope="col">Semester</th>
                                <th scope="col">Reg. No.</th>                                    
                                <th scope="col">Student name</th>
                                <th scope="col">Credit</th>
                                <th scope="col">Graduation</th>
                                <th scope="col">Exam1</th>
                                <th scope="col">Section</th>
                                <th scope="col">Short Name</th>
                                <th scope="col">Degeree</th>
                                <th scope="col">Year</th> 
                                <th scope="col">Student Signature</th>         
                            </tr>
                            </thead>
                            <tbody>
                                {this.props.data.map((row,index)=>{
                                    return(
                                        <tr key={index}>
                                            <td></td>
                                            <td>{row["Batch"]}</td>
                                            <td>{row["Regulation"]}</td>
                                            <td>{row["Program"]}</td>
                                            <td>{row["Course Code"]}</td>
                                            <td>{row["Course Name"]}</td>
                                            <td>{row["Semester"]}</td>
                                            <td>{row['Roll Number']}</td>
                                            <td>{row["Student Name"]}</td>
                                            <td>{row["Credit"]}</td>
                                            <td>UG</td>
                                            <td>ODD 2019</td>
                                            <td>{row["Section"]}</td>
                                            <td>{row["short_name"]}</td>
                                            <td>{row["Degeree"]}</td>             
                                            <td>{row["Year of Studying"]}</td> 
                                             <td></td>                                                        
                                        </tr>
                                    )
                                })}
                            </tbody>
                        </table>
                        
                    </React.Fragment>
                    )
                }}
            </Consumer>
        )
    }
}
export default Table;