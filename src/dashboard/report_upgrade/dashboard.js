import React from 'react';
import {Consumer} from "../../ContextEngine";
import Card from '../card';
import project from '../api';
import {
    faChartBar, faClock, faFileAlt,faTasks, faUser,
    faUserCircle,faExclamationTriangle,faGraduationCap,faInfoCircle,faChartLine
} from "@fortawesome/fontawesome-free-solid/index.es";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Table from './Table'
import './report.css';



class ReportDash extends React.Component{
    state = {
        table : false
    }

    render()
    {
        return(
            <Consumer>
                {(consumer)=>{
                    return(
                        <div id={"padding1"} className={"container-fluid"}>
                        <div className={"row card border border-top-0 border-bottom-0 border-light bg-light" }>
                            <div id={"padding-all"} className={"col-12"}>
                                <h4 className={"card-body display-4"} align="center"> <FontAwesomeIcon icon={faChartLine}/>Generate Report
                                </h4>
                                <hr/>
                                <div className={"container"}>
                                <form onSubmit={consumer.LoadSelectedCourses}>
                                            <div className={"form-row"}>                                                
                                                <div className="form-group col-md-5 offset-md-3">
                                                    <label htmlFor="inputState">Select Report Type</label>
                                                    <select  className={"form-control"} name={"report_view"} onChange={async (e)=>{await consumer.onChangeReportView(e);}}>
                                                        <option value={"0"}>Select Type Of Report</option>
                                                        <option value={"1"}>General Report</option>
                                                        <option value={"2"}>Not Selected Students</option>
                                                        <option value={"3"}>Course Wise Report</option>
                                                        <option value={"4"}>Course Status Report</option>
                                                    </select>
                                                </div>                                                
                                            </div>
                                            <div className={"form-row"}>
                                                <div className={consumer.state.reportVisibility.department?'show col':'hide'}>
                                                <select  className={"form-control"} name={"department"} onChange={(e)=>consumer.onChangeDepartment(e)}>
                                                        <option value={"0"}>Department</option>
                                                        {consumer.state.departments.map((dap =>{
                                                            return <option key={dap.dep_id} value={dap.dep_id}>{dap.dep_name}</option>
                                                        }))}
                                                </select>
                                                </div>
                                                <div className={consumer.state.reportVisibility.sem?'show col':'hide'}>
                                                <select  className={"form-control"} name={"sem"} onChange={(e)=>consumer.onChangeSemester(e)}>
                                                        <option value={null}>Semester</option>
                                                        {consumer.state.sems.map((sem,index)=>{
                                                            return <option key={index} value={sem.sem}>{sem.sem}</option>
                                                        })}
                                                </select>
                                                </div>
                                                <div className={consumer.state.reportVisibility.sec?'show col':'hide'}>
                                                <select  className={"form-control"} name={"sec"} onChange={(e)=>consumer.onChangeSection(e)}>
                                                        <option value={null}>Sec</option>
                                                        {consumer.state.secs.map((sec,index)=>{
                                                            return <option key={index} value={sec.sec}>{sec.sec}</option>
                                                        })}
                                                </select>
                                                </div>
                                               
                                               <div className={consumer.state.reportVisibility.etype?'show col':'hide'}>
                                                <select  className={"form-control"} name={"type"} onChange = {(e)=>consumer.onChangeType(e)}>
                                                        <option value={null}>Elect Type</option>
                                                        <option value={"1"}>Professional Elective</option>
                                                        <option value={"2"}>Open Elective</option>                                                        
                                                </select>
                                                </div>
                                                <div className={consumer.state.reportVisibility.etype?'show col':'hide'}>
                                                <select  className={"form-control"} name={"type"} onChange = {(e)=>consumer.onChangeSlot(e)}>
                                                        <option value={null}>Slot</option>
                                                        {consumer.state.grps.map((grp,index)=>{
                                                            return <option key={index} value={grp.grp_name}>{grp.grp_name}</option>
                                                        })}
                                                                                                               
                                                </select>
                                                </div>
                                                <div className={consumer.state.reportVisibility.course?'show col':'hide'}>
                                                <select  className={"form-control"} name={"course"} onChange={consumer.onChangeCourse}>
                                                        <option value={null}>Course</option>
                                                        {consumer.state.availableCourses.map((course,index)=>{
                                                            return <option key={index} value={course.course_id}>{course.course_code +" "+course.course_name}</option>
                                                        })}
                                                </select>
                                                </div>
                                                <div className={consumer.state.reportVisibility.catagery?'show col':'hide'}>
                                                <select  className={"form-control"} name={"catogery"}>
                                                        <option value={null}>Catogery</option>
                                                        <option value={"0"}>All</option>
                                                        <option value={"1"}>Own</option>
                                                        <option value={"2"}>Other</option>
                                                </select>
                                                </div>
                                            </div>
                                        </form>
                                        <div className={'d-flex justify-content-center mt-3'}>
                                            <input type="button" className={'btn btn-success'} value="Load" onClick={async(e)=>{await consumer.onLoadReportData(e)}}/>
                                        </div>
                                </div>
                            </div>
                            <div className={'container-fluid'}>                                  
                            {consumer.state.reportTable ?  <Table data={consumer.state.report}/> : <div></div>}
                            </div>
                        </div>
                        
                        </div>
                    )
                }}
            </Consumer>
        )
    }
}

export default ReportDash;