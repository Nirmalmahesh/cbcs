const PDF = {
    orientation: 'landscape',
    pageSize: 'A3',
    extend: 'pdfHtml5',
    text: 'PDF'

}
export default PDF;