const COLVIS = {
    extend: 'colvis',
    columnText: function (dt, idx, title) {
        return (idx + 1) + ': ' + title;
    }
}

export default COLVIS;