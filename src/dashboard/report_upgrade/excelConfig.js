const EXCEL = {
    extend: 'excel',
    exportOptions: {
        columns: ':visible'
    }
}
export default EXCEL;