import React from 'react';
import {faCheck, faExclamationTriangle} from "@fortawesome/fontawesome-free-solid/index.es";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import ReactLoading from 'react-loading';
import project from '../api'
class TableData extends React.Component {
    async componentWillMount(){
        await this.checkStatus();
    }
    constructor(props)
    {
        super(props);
    this.state = {
        status:"",
        text:"",
        time : 0,
        isSelect : "",
        need : "",
        Interval: "",
        grp_name : props.data.grp_name
    };
}

async checkStatus()
{
        if (this.state.grp_name != "" && this.state.grp_name != null) {
                    const response = await fetch(`${project}/api/CheckStatusFinal`, {
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                        },
                        method: "post",
                        body: JSON.stringify({
                            grp_name: this.state.grp_name,
                            username: sessionStorage.getItem('user')
                        })
                    }).catch((err) => {
                        console.log(err.message);
                    });
        
                    try {
    
                        const dataFinal = await response.json();
                    console.log(dataFinal[0]);
                        if (dataFinal[0].status == 1) {
                            console.log("success");
                            this.setState({status: "success"});
                            this.setState({text: "Enrolled"});
                            sessionStorage.setItem('status', 'Enrolled');
                        } else {
                            this.props.Timefun();
                              sessionStorage.setItem('status', 'warning');
                            this.setState({status: "warning"});
                            this.setState({text: "Waiting List"});
                          
                        }
                    } catch (e) {
                        console.log("FAILED");
                    }
                }
}
    async update()
    {
        const responseAvailable = await fetch(`${project}/api/checkStudentAvailable`,{
            headers :{
                'Content-Type': 'application/json',
                    'Accept': 'application/json',
            },
            method : "post",
            body : JSON.stringify({
                grp_name: this.state.grp_name,
                username: sessionStorage.getItem('user')
            })
        }).catch((err) => {
            console.log(err.message);
        });


        try {
            const dataAvailable = await responseAvailable.json();
            
            if (dataAvailable.length >= 1) {

                
              this.setState({isSelect : true});
               
            } else {
                this.setState({isSelect : false});
            }
        } catch (e) {
            console.log("FAILED");
        }






        console.log("Status :"+this.state.status);

        if(this.state.isSelect)
        {
            const responseTime = await fetch(`${project}/api/CheckTimeExpire`,{
                headers :{
                    'Content-Type': 'application/json',
                        'Accept': 'application/json',
                },
                method : "post",
                body : JSON.stringify({
                    grp_name: this.state.grp_name,
                    username: sessionStorage.getItem('user')
                })
            }).catch((err) => {
                console.log(err.message);
            });
    
    
            try {
                const dataTime = await responseTime.json();
                console.log("Time:"+dataTime[(dataTime.length)-1].minute);
                if (dataTime[(dataTime.length)-1].minute <= 15) {
    
                    
                   this.setState({time:1})
                   
                } else {
                    this.setState({time:0})
                }
            } catch (e) {
                console.log("FAILED");
            }
    
            
            console.log("State Time:"+this.state.time);
            if(this.state.time)
            {
                
                if (this.state.grp_name != null && this.state.grp_name !== "") {
                   
                    const response = await fetch(`${project}/api/CheckStudentWaiting`, {
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                        },
                        method: "post",
                        body: JSON.stringify({
                            grp_name: this.state.grp_name,
                            username: sessionStorage.getItem('user')
                        })
                    }).catch((err) => {
                        console.log(err.message);
                    });
                    
                    try {
                        const data = await response.json();                       
                        if (data.length >= 1) {
                            this.setState({status: "success"});
                            this.setState({text: "Enrolled"});
                            sessionStorage.setItem('status', 'Enrolled');
                        } else {
                            
                            this.setState({status: "warning"});
                            this.setState({text: "Waiting List"});
                            sessionStorage.setItem('status', 'warning');
                        }
                    } catch (e) {
                        console.log("FAILED");
                    }
                }
            }else{   //this.props.set.has(this.props.data.grp_name)                          
                                         
    
            await this.checkStatus();
    
            }
            if (this.state.time) {
                console.log("Making It Final");
                                const responseFinal = await fetch(`${project}/api/MakeItfinal`, {
                                    headers: {
                                        'Content-Type': 'application/json',
                                        'Accept': 'application/json',
                                    },
                                    method: "get",
                                   
                                }).catch((err) => {
                                    console.log(err.message);
                                });
                                await this.checkStatus();
                            }    
            if(this.state.status === 'warning')
            {              

                const need = await fetch(`${project}/api/need`,{
                    headers : {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json'
                    },
                    method : "post",
                    body : JSON.stringify({
                        grp_name: this.state.grp_name,
                        username: sessionStorage.getItem('user')
                    })
                })
                try {
                    const responseneed = await need.json();
        
                    this.setState({
                        need : responseneed[0].need
                    })
                    
                }catch (e) {
                    console.log("FAILED");
                }
                 if(this.state.need<=0 && this.state.need !== '' && this.state.need !== null)
                {
                    
                     const response = await fetch(`${project}/api/UpdateStatus`, {
                        headers: {
                            'Content-Type': 'application/json',
                            'Accept': 'application/json',
                        },
                        method: "post",
                        body: JSON.stringify({
                            grp_name: this.state.grp_name,
                            username: sessionStorage.getItem('user')
                        })
                    }).catch((err) => {
                        console.log(err.message);
                    });
                }
                await this.checkStatus();
                 
            }
        }
        
    }
    async componentWillMount(){
        this.setState({isLoading:true});    

        this.update();
        this.setState({Interval : setInterval(this.update.bind(this),20000)})
         
      
        
        this.setState({isLoading:false});
    }
    componentDidUpdate()
    {
        if(this.state.status === 'success')
        {
            clearInterval(this.state.Interval)
        }
    }
    componentWillUnmount(){
        clearInterval(this.state.Interval);
    }
    render() {
        const {data,isLoading} = this.props;

        return (
            <React.Fragment>
            <tr align="center">
                <td><h6><em>{data.grp_name}</em></h6></td>              
                
                {isLoading ?
                <div className={'row justify-content-md-center'}>
                    <div className={"col"} align="center">
                        <ReactLoading type={'bars'} delay={0} color={'#2980b9'}/>
                        <h4>Loading...</h4>
                    </div>
                </div> : <td> {this.state.status === 'warning' || this.state.status === 'success' ?  <h5>{this.state.status === "warning" ?
                    <button key={data.grp_name}                    
                            value={data.grp_name}
                            onClick={this.props.onHandleClickLsnr}
                            name={`list_type_${this.props.type}`}
                            className={"btn btn-warning"}><FontAwesomeIcon icon={this.state.status ==="success" ? faCheck : faExclamationTriangle }/> Waiting List
                    </button> : <span className={`badge badge-${this.state.status} `}><FontAwesomeIcon icon={this.state.status ==="success" ? faCheck : faExclamationTriangle }/> <em>{this.state.text}</em>
                </span>}</h5> :
                    <button key={data.grp_name}
                          value={data.grp_name}
                          onClick={this.props.onHandleClickLsnr}
                          name={`list_type_${this.props.type}`}
                          className={"btn btn-primary"}>Enroll
                </button>}
                </td>}
            </tr>
            <tr align="center">
              { this.state.status === "warning" && this.state.time === 0?
                <React.Fragment>
                <td><h6><em>Number of students need for course get conform </em></h6></td>
                <td>{this.state.need}</td>
                </React.Fragment> :<h3></h3> }      
            </tr>
            </React.Fragment>
        );
    }
}

export default TableData;