import React, { Component } from 'react';
import {
    faExclamationTriangle
} from "@fortawesome/fontawesome-free-solid/index.es";
import Loader from "../loader/loader";
import AlertNotify from "../alert";
import {Consumer} from "../../ContextEngine";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import TableSelectedCourses from './table'

class ManageSelection extends Component {

    state={
        isLoading:false
    }
    componentWillMount()
    {
        this.setState({
            isLoading:true
        })
    }
    componentDidMount()
    {
        this.setState({
            isLoading:false
        })
    }
    render() {
        return (
            <Consumer>
                {(consumer)=>{
                    return(
                        <React.Fragment>
                            {consumer.state.isLoading && <Loader/>}
                            <div id={"padding1"} className={"container "}>
                                <div className={"row card border border-top-0 border-bottom-0 border-light bg-light" }>
                                    <div id={"padding-all"} className={"col-12"}>
                                        <h4 className={"card-body display-4"} align="center"> <FontAwesomeIcon icon={faExclamationTriangle}/>Manage Selection
                                        </h4>
                                        <hr/>
                                        <form onSubmit={consumer.LoadSelectedCourses}>
                                            <div className={"form-row"}>
                                                <div className="form-group col-md-5">
                                                    <label htmlFor="inputState">Enter Roll Number</label>
                                                    <input type={"text"} className={"form-control"} placeholder={"Enter Rollnumber"} name={"rollNumber"} onChange={(e)=>consumer.OnchangeChangeCourse(e)}/>
                                                </div>
                                                <div className="form-group col-md-5">
                                                    <label htmlFor="inputState">Select Elective Type</label>
                                                    <select  className={"form-control"} name={"elect_group"} onChange={(e)=>consumer.OnchangeChangeCourse(e)}>
                                                        <option value={"0"}>Please Select</option>
                                                        <option value={"2"}>OPEN ELECTIVE</option>
                                                        <option value={"1"}>PROFESSIONAL ELECTIVE</option>
                                                    </select>
                                                </div>
                                                <div className={"form-group col-md-2"}>
                                                    <br/>
                                                    <input type={"submit"} value={"Search"} className={"btn btn-primary"} name={"btn_selection_list"} />
                                                </div>
                                            </div>
                                        </form>
                                        <br/>
                                        <hr/>

                                        <h4 className={'text-center'}>Change Course</h4>
                                    </div>
                                    <div className={'row'}>
                                        <div className={'col-md-6'}>
                                            <TableSelectedCourses/>
                                            <div className={'row justify-content-center'}>
                                            <button className={'btn btn-primary mb-3 '} align={'center'} onClick={consumer.loadAlternateCourses}>Load Alternate Courses</button>
                                            </div>
                                        </div>
                                        <div className={'col-md-5'}>
                                            <form onSubmit={consumer.changeCourse} >
                                                <div className={'form-group'}>
                                                    <div className="form-group">
                                                        <label htmlFor="select_new_course">Select New Course</label>
                                                        <select className="form-control" name={'new_course'} onChange={consumer.OnchangeChangeCourse}>
                                                            <option value={'0'}>Select New Course</option>
                                                            {consumer.state.availableForUpdateCourse.map((course =>{
                                                                return <option key={course.course_id} value={course.course_id}>{course.course_name}</option>
                                                            }))}
                                                        </select>
                                                        <div className={'row justify-content-center'}>
                                                            <input type={'submit'} value={'Change'} className={'btn btn-success mt-3'}  />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    )
                }}
            </Consumer>
        );
    }
}

export default ManageSelection;
