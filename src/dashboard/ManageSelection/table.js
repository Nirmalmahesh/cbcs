import React, { Component } from 'react';
import '../../css/main.css'
import {Consumer} from "../../ContextEngine";

class TableSelectedCourses extends Component {
    render() {
        return (
            <Consumer>
                {(consumer)=>{
                    return(
                        <div id={'padding-all'} className={"container"}>
                            <div id={'overflow'} className={"border border-secondary rounded"}>
                                <table id="MyTable" className="table table-hover table-bordered" cellSpacing="0" width="100%">
                                    <thead className={"thead-light"}>
                                    <tr>
                                        <th>Check</th>
                                        <th>Course Code</th>
                                        <th>Course Name</th>
                                        <th>Group Name</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {consumer.state.selectedCourses.open.map(( course =>{

                                        return  (
                                            <tr>
                                                <td><input key={course.course_id} className="input-group" value={course.choose_id} name="cheackBoxSelectedCourse" onChange={consumer.checkBoxInputChange}
                                                 type="checkbox" /></td>
                                                <td>{course.course_code}</td>
                                                <td>{course.course_name}</td>
                                                <td>{course.grp_name}</td>

                                            </tr>)
                                    }))}
                                    </tbody>
                                </table></div>
                        </div>
                    )
                }}
            </Consumer>
        );
    }
}

export default TableSelectedCourses;
