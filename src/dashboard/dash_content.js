import React from 'react';
import '../css/main.css';
import Card from "./card";
import {
    faChartBar, faClock, faFileAlt,faTasks, faUser,
    faUserCircle,faExclamationTriangle,faGraduationCap,faInfoCircle
} from "@fortawesome/fontawesome-free-solid/index.es";
import project from './api'
class DashCont extends React.Component {
    

   async crack()
    {
        const response_check = await fetch(`${project}/api/CrackAllData123`, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            method:"get",
           
        }).catch((err) => {
            console.log(err.message);
        });
        
    }

    render(){
        return(
            <div id={"padding-all"}>
                <div className={"row justify-content-md-center"}>
                    <Card cardTitle={"Department Users"} srcImg={faUserCircle} linkURL={`${project}/dashboard/department`} Imgheight={"3x"}/>
                    <Card cardTitle={"Students List"} srcImg={faUser} linkURL={`${project}/dashboard/students-list`} Imgheight={"3x"}/>
                    <Card cardTitle={"Set Timing"} srcImg={faClock} linkURL={`${project}/dashboard/timing`} Imgheight={"3x"}/>
                    <Card cardTitle={"Add Staff"} srcImg={faUserCircle} linkURL={`${project}/dashboard/add_staff`} Imgheight={"3x"}/>
                    <Card cardTitle={"Group Courses"} srcImg={faFileAlt} linkURL={`${project}/dashboard/group_courses`} Imgheight={"3x"}/>
                    <Card cardTitle={"Combine Courses"} srcImg={faTasks} linkURL={`${project}/dashboard/combine_courses`} Imgheight={"3x"}/>
                    <Card cardTitle={"Manage Courses"} srcImg={faGraduationCap} linkURL={`${project}/dashboard/manage_course`} Imgheight={"3x"}/>
                    <Card cardTitle={"Report"} srcImg={faChartBar} linkURL={`${project}/dashboard/report`} Imgheight={"3x"}/>
                    <Card cardTitle={"Manage Selection"} srcImg={faExclamationTriangle} linkURL={`${project}/dashboard/manage_selection`} Imgheight={"3x"}/>
                    <Card cardTitle={"Course Status"} srcImg={faInfoCircle} linkURL={`${project}/dashboard/course_status`} Imgheight={"3x"}/>
                    
                </div>
                <button type="button" className={'btn btn-danger'} onClick={this.crack}>Delete All data</button>
            </div>
        );
    }
}

export default DashCont;