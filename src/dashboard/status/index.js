import React, { Component } from 'react';
import {
    faExclamationTriangle,faInfoCircle
} from "@fortawesome/fontawesome-free-solid/index.es";
import Loader from "../loader/loader";
import AlertNotify from "../alert";
import {Consumer} from "../../ContextEngine";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import StatusTable from './statusTable'

class CourseStatus extends Component {

    state={
        isLoading:false
    }
    componentWillMount()
    {
        this.setState({
            isLoading:true
        })
    }
    componentDidMount()
    {
        this.setState({
            isLoading:false
        })
    }
    render() {
        return (
            <Consumer>
                {(consumer)=>{
                    return(
                        <React.Fragment>
                            {consumer.state.isLoading && <Loader/>}
                            <div id={"padding1"} className={"container "}>
                                <div className={"row card border border-top-0 border-bottom-0 border-light bg-light" }>
                                    <div id={"padding-all"} className={"col-12"}>
                                        <h4 className={"card-body display-4"} align="center"> <FontAwesomeIcon icon={faInfoCircle}/>Course Status
                                        </h4>
                                        <hr/>

                                        <StatusTable/>

                                    </div>
                                </div>
                            </div>
                        </React.Fragment>
                    )
                }}
            </Consumer>
        );
    }
}

export default CourseStatus;
