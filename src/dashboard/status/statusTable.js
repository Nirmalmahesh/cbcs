import React, { Component } from 'react';
import '../../css/main.css'
import {Consumer} from "../../ContextEngine";




class StatusTable extends Component {

    state ={
        data : null,
        loadData:""
    }
    /*componentDidMount()
    {
        this.setInterval(this.loadData,5000);
    }
    componentWillUnmount()
    {
        this.clearInterval();
    }
*/


    render() {
        return (
            <Consumer>
                {(consumer)=>{
                    return(
                        <div id={'padding-all'} className={"container"}>
                            <div id={'overflow'} className={"border border-secondary rounded"}>
                                {/*{this.setState({loadData:consumer.loadStatusCourse})}*/}
                                <input type={"button"} className={"btn btn-primary"} onClick={consumer.loadStatusCourse}  value={"Refresh"}/>
                                <table id="MyTable" className="table table-hover table-bordered table-responsive" cellSpacing="0" width="100%">
                                    <thead className={"thead-light"}>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Course Code</th>
                                        <th>Course Name</th>
                                        <th>Course Belongs To</th>
                                        <th>Semester</th>
                                        <th>Group Name</th>
                                        <th>Elect Type</th>
                                        <th>Maximum Students Per Own Department</th>
                                        <th>Seats Taken By Own Department</th>
                                        <th>Seats Available for own Department</th>
                                        <th>Maximum Students Per Other Department</th>
                                        <th>Seats Taken By Other Department</th>
                                        <th>Seats Available for Other Department</th>
                                        <th>Total Seats</th>
                                        <th>No Of Seats Occupied</th>
                                        <th>Number Seats Available</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    {consumer.state.courseStatus.map((course)=>{
                                        return(
                                        <tr>
                                            <td></td>
                                            <td>{course.course_code}</td>
                                            <td>{course.course_name}</td>
                                            <td>{course.dep_name}</td>
                                            <td>{course.sem}</td>
                                            <td>{course.grp_name}</td>
                                            <td>{course.elect_id}</td>
                                            <td>{course.own_max_lim}</td>
                                            <td>{course.own_count}</td>
                                            <td>{course.own_vacancy}</td>
                                            <td>{course.max_lim_other}</td>
                                            <td>{course.other_count}</td>
                                            <td>{course.vacancy_other}</td>
                                            <td>{course.total_seats}</td>
                                            <td>{course.total_filled}</td>
                                            <td>{course.total_vacancy}</td>

                                        </tr>
                                        )
                                    })}
                                    </tbody>
                                </table></div>
                        </div>
                    )
                }}
            </Consumer>
        );
    }
}

export default StatusTable;
