import React from 'react';
import "../../../css/main.css"
import 'datatables.net-bs4/css/dataTables.bootstrap4.min.css';
import 'datatables.net-bs4/js/dataTables.bootstrap4'
import 'datatables.net-buttons-bs4'
import 'datatables.net-buttons-bs4/js/buttons.bootstrap4'

import {faChartBar, faCheck, faExclamationTriangle} from "@fortawesome/fontawesome-free-solid/index.es";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import MediaListCourse from "./media_list";
import Loader from "../../loader/loader";
import * as XLSX from "xlsx";
import AlertNotify from "../../alert";
import  * as jsPDF  from 'jspdf'
import * as html2canvas from "html2canvas";
import project from '../../api'
const $=require('jquery')

$.Datatable =require('datatables.net-bs4');
$.Datatable=require( 'datatables.net-buttons/js/buttons.html5.js' );
$.Datatable=require('datatables.net-buttons/js/buttons.colVis');


class Report extends React.Component {

    withstaff = () =>{
        console.log(this.el);
        this.$el =$(this.el);
       var t= this.$el.DataTable({
            lengthChange: false,
            destroy:true,
            adjust:true,
            columnDefs:[
                {
                    targets:[6,7,8,9,10],
                    visible:false
                },
                {
                    "searchable": false,
                    "orderable": false,
                    "targets": 0
                }
            ],
            "order": [[ 1, 'asc' ]],
            buttons: [
                
               
                {extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                pageSize: 'A4',
                customize : function(doc) {doc.pageMargins = [20, 50, 50,50 ]; },
                exportOptions: {
                    columns: ':visible'
                }
                },
                {
                    extend: 'colvis'
                }
                
            
             ],
            dom: 'Bfrtip'

        }
        );
        t.on( 'order.dt search.dt', function () {
            t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
    } 
    

    componentWillUpdate()
    {
        this.withstaff();
    }
    

    state= {
        depart:[],
        dep_id:"",
        sem:[3,4,5,6,7,8],
        sec:[],
        elect:"",
        section:"",
        student_count:"",
        semester:"",
        course_id:"",
        errors:{
            title:"",
            message:"",
            type:"",
            icon:""
        },
        CoursesAvail:[],
        export_detail:[],
        export:[],
        display:false,
        display1:false,
        view:false,
        without:false
    };
    onSubmit2 = (e) => {
        e.preventDefault();
        if(e.target.name === 'ok'){
            this.myCall(e.target.name);
        }if(e.target.name === 'export'){
            this.myCall('export');
        }if(e.target.name === 'pdf'){
            this.setState({without:false});
            this.myCall('export','pdf');
        }if(e.target.name === 'pdf_without'){
            this.setState({without:true});
            this.setState({display1:true});
            this.setState({display:false});
            this.myCall('without','pdf');
        }if(e.target.name === 'without'){
            this.myCall('without');
        }
    };
    onChange = (e) =>{
        if(e.target.name === 'depart'){
            this.setState({dep_id:e.target.value});
            this.myCall('semester',e.target.value);
        }else if(e.target.name === 'sem'){
            this.setState({semester:e.target.value});
            this.myCall('section',e.target.value);
        }else if(e.target.name === 'course'){
            this.setState({course_id:e.target.value});
        }else if(e.target.name === 'sec'){
            this.setState({section:e.target.value});
        }else if(e.target.name === 'elect'){
            this.setState({elect:e.target.value});
            if(e.target.value === '1' ){
                this.setState({elect_name:"Professional Elective"});
            }else{
                this.setState({elect_name:"Open Elective"});
            }
        }
    };

    async myCall(name,value=''){
        if(name === 'ok') {
            this.setState({isLoading: true});
            this.setState({display:true});
            this.setState({display1:false});
            const response = await fetch(`${project}/api/courses_list`, {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                method:"post",
                body:JSON.stringify({dep_id:this.state.dep_id,sem:this.state.semester,elect:this.state.elect})
            }).catch((err) => {
                console.log(err.message);
            });

            try {
                const data = await response.json();
                localStorage.setItem('CoursesAvail',JSON.stringify(data));
                this.setState({CoursesAvail:JSON.parse(localStorage.getItem('CoursesAvail'))});
                sessionStorage.setItem('report','load');

                const response2 = await fetch(`${project}/api/StudCount`, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                    },
                    method:"post",
                    body:JSON.stringify({depid:this.state.dep_id,sem:this.state.semester})
                }).catch((err) => {
                    console.log(err.message);
                });

                try {
                    const data2 = await response2.json();
                    console.log(data2[0]);
                    this.setState({student_count:data2[0].cnt});
                } catch (e) {
                    this.setState({isLoading: false});
                    console.log("FAILED");
                }

                this.setState({isLoading: false});
            } catch (e) {
                this.setState({isLoading: false});
                console.log("FAILED");
            }
        }else if (name === 'semester'){
            this.setState({isLoading: true});
            const response = await fetch(`${project}/api/${name}`, {
                headers : {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                method:"post",
                body:JSON.stringify({dep_id:value})
            }).catch( (err) =>{
                console.log(err.message);
            });

            try {
                const data = await response.json();
                this.setState({sem: data});
                this.setState({isLoading:false});
            } catch (e) {

                this.setState({isLoading:false});
                console.log("FAILED");
            }

        }else if(name === 'section') {
            this.setState({isLoading: true});
            const response = await fetch(`${project}/api/${name}`, {
                headers : {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                method:"post",
                body:JSON.stringify({depid:this.state.dep_id,sem:value})
            }).catch( (err) =>{
                console.log(err.message);
            });

            try {
                const data = await response.json();
                this.setState({sec: data});
                this.setState({view: true});
                this.setState({isLoading:false});

            } catch (e) {

                console.log("FAILED");
            }
        }else if (name === 'export'){
            this.setState({isLoading: true});
            const response = await fetch(`${project}/api/export`, {
                headers : {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                method:"post",
                body:JSON.stringify({
                    sem:this.state.semester,
                    course_id:this.state.course_id,
                    dep_id:this.state.dep_id,
                    sec:this.state.section,
                    elect:this.state.elect
                })
            }).catch( (err) =>{
                console.log(err.message);
            });
            try {
                const data = await response.json();
                if (data.length !== 0 && value !=='pdf') {
                    this.setState({
                        errors:{
                            title:"Super !!",
                            message:"You Can Download as Excel",
                            type:"info",
                            icon:faCheck
                        }
                    });
                    console.log(data);
                    const xls = XLSX.utils.json_to_sheet(Array.from(data));
                    console.log(data);
                    const workbook = {Sheets: {'data': xls}, SheetNames: ['data']};
                    XLSX.writeFile(workbook, 'Export.xlsx', {bookType: 'xlsx', type: 'binary'});
                }if(data.length !== 0 && value ==='pdf'){
                    this.setState({
                        export_detail:data
                    })
                }else{
                    this.setState({
                        errors:{
                            title:"Cannot Export",
                            message:"No Records Found !!",
                            type:"danger",
                            icon:faExclamationTriangle
                        }
                    });
                }
                this.setState({isLoading:false});
            } catch (e) {

                this.setState({isLoading:false});
                console.log("FAILED");
            }
        }else if (name === 'without'){
            console.log("Sem :"+ this.state.semester);
            console.log("Course Id :"+this.state.course_id);
            console.log("Dep Id :"+this.state.dep_id);
            console.log("Sec :"+this.state.section);
            console.log("elect :"+this.state.elect);
            this.setState({isLoading: true});
            const response = await fetch(`${project}/api/export`, {
                headers : {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                method:"post",
                body:JSON.stringify({
                    sem:this.state.semester,
                    course_id:this.state.course_id,
                    dep_id:this.state.dep_id,
                    sec:this.state.section,
                    elect:this.state.elect
                })
            }).catch( (err) =>{
                console.log(err.message);
            });
            try {
                const data = await response.json();
                if (data.length !== 0 && value !=='pdf') {
                    this.setState({
                        errors:{
                            title:"Super !!",
                            message:"You Can Download as Excel",
                            type:"info",
                            icon:faCheck
                        }
                    });
                    const xls = XLSX.utils.json_to_sheet(Array.from(data));
                    console.log(data);
                    const workbook = {Sheets: {'data': xls}, SheetNames: ['data']};
                    XLSX.writeFile(workbook, 'export.xlsx', {bookType: 'xlsx', type: 'binary'});
                }if(data.length !== 0 && value ==='pdf'){
                    this.setState({
                        export_detail:data
                    })
                }else{
                    this.setState({
                        errors:{
                            title:"Cannot Export",
                            message:"No Records Found !!",
                            type:"danger",
                            icon:faExclamationTriangle
                        }
                    });
                }
                this.setState({isLoading:false});
            } catch (e) {

                this.setState({isLoading:false});
                console.log("FAILED");
            }
        }
    }

    onPrint = (e) =>{
        const input = document.getElementById('divToPrint');
        html2canvas(input)
            .then((canvas) => {
                const imgData = canvas.toDataURL('image/png');
                const pdf = new jsPDF();
                pdf.addImage(imgData, 'JPEG', 0, 0);
                pdf.save("download.pdf");
            })
        ;
    };

    async componentWillMount(){
        const response2 = await fetch(`${project}/api/department`, {
            headers : {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
        }).catch( (err) =>{
            console.log(err.message);
        });

        try {
            const data = await response2.json();
            this.setState({depart:data});
            this.setState({isLoading:false});
        } catch (e) {
            this.setState({isLoading:false});
            console.log("FAILED");
        }
    }

    render() {
        var sno=0;
        const {CoursesAvail,errors} = this.state;
        return (
            <div id={"padding-all"}><div id={"padding1"} >{this.state.isLoading && <Loader/>}
                <div className={"card border border-top-0 border-bottom-0 border-light bg-light"}>
                    <div  className={"card-body"}>
                        <div className={"row justify-content-md-center " }>
                            <div id={"padding-all"} className={"col-12"} align="center">
                                <h2 className={"card-body"}><em><FontAwesomeIcon icon={faChartBar}/> Report </em></h2><hr/>
                            </div>
                        </div>
                        {errors.title !== "" && <AlertNotify icon={errors.icon} title={errors.title} type={errors.type} message={errors.message} />}
                        <div id={"this"} className={'row justify-content-md-center'} align="center">
                            <div className={"col-lg-3 col-md-12"}>
                                <div id={"department"} className="input-group mb-3">
                                    <select className="custom-select" name="depart" onChange={this.onChange}>
                                        <option value="">Department</option>
                                        {this.state.depart.map((dap =>{
                                            return <option key={dap.dep_id} value={dap.dep_id}>{dap.dep_name}</option>
                                        }))}
                                    </select>
                                </div>
                            </div>
                            <div className={"col-lg-2 col-md-12"}>
                                <div id={"semester"} className="input-group mb-3">
                                    <select className="custom-select" name="sem" onChange={this.onChange}>
                                        <option value="">Sem</option>
                                        {this.state.sem.map((dap =>{
                                            return <option key={dap.sem} value={dap.sem}>{dap.sem}</option>
                                        }))}
                                    </select>
                                </div>
                            </div>
                            <div className={"col-lg-3 col-md-12"}>
                                <div id={"semester"} className="input-group mb-3">
                                    <select className="custom-select" name="elect" onChange={this.onChange}>
                                        <option value="">Elective Type</option>
                                        <option value="1">Professional Elective</option>
                                        <option value="2">Open Elective</option>
                                    </select>
                                </div>
                            </div>
                            <div className={"col-lg-1 col-md-12"}>
                                <div id={"semester"} className="input-group mb-3">
                                    <button name="ok" onClick={this.onSubmit2} className={"btn btn-success"}>OK</button>
                                </div>
                            </div>
                        </div>
                            <div className={"row justify-content-md-center"}>
                                <div id={"section"} className="input-group mb-3 col-lg-2 col-md-12"  onChange={this.onChange}>
                                    <select className="custom-select" name="sec">
                                        <option value="">Section</option>
                                        {this.state.sec.map((dap =>{
                                            return <option value={dap.sec}>{dap.sec}</option>
                                        }))}
                                    </select>
                                </div>
                                <div className={"col-lg-3 col-md-12"}>
                                    <div id={"semester"} className="input-group mb-3">
                                        <select className="custom-select" name="course" onChange={this.onChange}>
                                            <option value="">Courses</option>
                                            {CoursesAvail.map((courses =>{
                                                return <option key={courses.course_id} value={courses.course_id}>{courses.course_code+" "+courses.course_name}</option>
                                            }))}
                                        </select>
                                    </div>
                                </div>
                                <div className={"col-lg-2 col-md-12"}>
                                <div id={"semester"} className="input-group mb-3">
                                    <button name="pdf_without" onClick={this.onSubmit2}
                                            data-toggle="modal" data-target="#example" className={"btn btn-primary"}>
                                        Export
                                    </button>
                                </div>
                            </div>
                            </div>
                        {this.state.student_count && <div className={"row justify-content-md-center"}>
                            {this.state.student_count && <div className={"col-lg-12 col-md-12"} align="center">
                                <h3>Total Students Count: {this.state.student_count}</h3></div>}
                        </div>}
                        
                                    {this.state.export_detail.length !==0 && this.state.display1 &&
                                        <div id="divToPrint" className="mt4" style={{
                                            backgroundColor: '#ffff',
                                            width: '100%',
                                            minHeight: '297mm',
                                            marginLeft: 'auto',
                                            marginRight: 'auto',
                                        }}>
                                                {
                                                    <table style={{
                                                        margi:'0 auto',
                                                        clear: 'both',
                                                        width:'auto',
                                                        layout:'fixed'
                                                    }} className="table table-striped table-bordered" ref={el => this.el =el}>
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">S. No.</th>
                                                                <th scope="col">Reg. No.</th>
                                                                <th scope="col">Student name</th>
                                                                <th scope="col">Section</th>
                                                                <th scope="col">Degeree</th>
                                                                <th scope="col">Program</th>
                                                                <th scope="col">Semester</th>
                                                                <th scope="col">Year</th>            
                                                                <th scope="col">Credit</th>
                                                                <th scope="col">Regulation</th>
                                                                <th scope="col">Batch</th>
                                                                <th scope="col">Course Code</th>
                                                                <th scope="col">Course Name</th>                                                                
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        {this.state.export_detail.map((rows)=>{
                                                            sno=sno+1;
                                                            return(
                                                                <tr>
                                                                    <td></td>
                                                                    <td>{rows["Roll Number"]}</td>
                                                                    <td>{rows["Student Name"]}</td>
                                                                    <td>{rows["Section"]}</td>
                                                                    <td>{rows["Degeree"]}</td>
                                                                    <td>{rows["Program"]}</td>
                                                                    <td>{rows["Semester"]}</td>
                                                                    <td>{rows["Year of Studying"]}</td>                                                                 
                                                                    <td>{rows["Credit"]}</td>
                                                                    <td>{rows["Regulation"]}</td>
                                                                    <td>{rows["Batch"]}</td>
                                                                    <td>{rows["Course Code"]}</td>
                                                                    <td>{rows["Course Name"]}</td>                                                                
                                                                </tr>
                                                            )
                                                        }) }
                                                        </tbody>
                                                    </table>}
                                        </div>}

                        {this.state.display && <div className="row justify-content-center" align="center">
                                {CoursesAvail.map((courses)=>{
                                    return <MediaListCourse courses={courses} key={courses.course_id} onUserInputChange={this.handleUserInputChange} />
                                })}
                                <hr/>
                                <br/>
                            </div>}
                    </div>
                </div>
                <br/>
            </div></div>
        );
    }
}

export default Report;