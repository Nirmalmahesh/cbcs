import React from 'react';
import "../../../css/main.css"

class ListCourseTbl extends React.Component {


    render(){
        var sno=0;
        return(
            <div  className={"container"} >
                <div id={"overflow2"}>
                <table id="MyTable" className="table table-hover table-bordered" cellSpacing="0" width="100%">
                    <thead className={"thead-light"}>
                    <tr>
                        <th>SNo.</th>
                        <th>Course Code</th>
                        <th>Course Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.props.table_data.map((data =>{
                        sno=sno+1;
                        return  (
                            <tr>
                                <td>{sno}</td>
                                <td>{data.course_code}</td>
                                <td>{data.course_name}</td>
                            </tr>)
                    }))}
                    </tbody>
                </table>
                </div>
            </div>
        );
    }
}

export default ListCourseTbl;