let State = {
    grps:[],
    departments:[],
    sems : [],
    secs:[],
    
    elect_type:"0",
    isLoading:"",
    username:"",
    studentDetails:{
      username:'',
      student_name:'',
      dep_id:'',
      sem:'',
      sec:'',
      year:'',
      type:'',
      program:'',
      regulation:'',
      batch:'',
    },
    errors:{
        title:"",
        message:"",
        type:"",
        icon:""
    },
    availableCourses:[],
    report:[],
    selectedCourses:{
        professional:[],
        open:[]
    },
    selectedCourseDetails:{
        username:"",
        elect_id:"",
        newCourse:''
    },
    courseStatus : [],
    reportData : {
        slot:null,
        dep_id : null,
        sem : null,
        sec:null,
        course_id : null,
        etype:null,
      
    },
    reportVisibility :{
        department : false,
        sem : false,
        sec : false,
        course : false,
        catagery : false,
    },
    reportTable:false,
    reportType:null,
    availableForUpdateCourse:[],
    selectedCheckboxes1 : new Set(),
    selectedCheckboxes2 : new Set()

    
}

export {State};