
import React from  "react";
import {Redirect} from "react-router";
import project from '../dashboard/api'
class Logout extends React.Component {
    render() {
        localStorage.clear();
        sessionStorage.clear();
        return <Redirect push to={project+'/'}/>;
    }
}

export default Logout;