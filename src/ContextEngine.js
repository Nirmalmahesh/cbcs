import React,{Component} from 'react';
import {State} from "./GlobalConfig";
import project from './dashboard/api';
import {Redirect} from 'react-router-dom'
const MainContext = React.createContext();

class Context extends Component{
    state=State;

   /* componentWillMount()
    {
        if(sessionStorage.getItem('user')!==null && sessionStorage.getItem('user')!=='' && sessionStorage.getItem('user'))
            const response = this.fetchData('/api/getStudentDetails',{username:sessionStorage.getItem('user')})
    }*/
    
    onChangeCourse = async(e) =>{
        await this.setState({reportData : {...this.state.reportData,course_id : e.target.value}})
    }
    onChangeType = async(e) =>{
        await this.setState({reportData : {...this.state.reportData,etype:e.target.value}});
        if(this.state.reportData.etype === '1')
        {
            await this.setState({grps:await this.fetchData('/api/gropingPro',{"dep_id":this.state.reportData.dep_id,"sem":this.state.reportData.sem})});
           
        }else if(this.state.reportData.etype === '2')
        {
            await this.setState({grps:await this.fetchData('/api/gropingOpen',{"dep_id":this.state.reportData.dep_id,"sem":this.state.reportData.sem})});           
        }
    }
    onChangeSlot = async(e) =>{
        await this.setState({reportData :{...this.state.reportData,slot : e.target.value}})
        await this.onLoadCourseData({"dep_id":this.state.reportData.dep_id,"sem":this.state.reportData.sem,"type":this.state.reportData.etype,"grp_name":this.state.reportData.slot});
    }
    onLoadCourseData = async(data) =>{
       
       await this.setState({availableCourses: await this.fetchData('/api/load_course_for_department',data)})

    }
    onLoadReportData = async(e)=>{
        await this.setState({report:await this.fetchData('/api/export',{sem:this.state.reportData.sem,course_id:this.state.reportData.course_id,dep_id:this.state.reportData.dep_id,sec:this.state.reportData.sec,elect:this.state.reportData.etype})});
        await this.setState({reportTable:true})
        
    }

    onChangeDepartment = async(e) =>{
        await this.setState({reportData : {...this.state.reportData,dep_id:e.target.value}})
        await this.setState({sems: await this.fetchData('/api/semester',{dep_id:this.state.reportData.dep_id})})
    }
    onChangeSemester = async(e) =>{
        await this.setState({reportData : {...this.state.reportData,sem:e.target.value}})
        await this.setState({secs: await this.fetchData('/api/section',{depid:this.state.reportData.dep_id,sem:this.state.reportData.sem})})    
        
    }
    onChangeSection =async(e) =>{
        await this.setState({reportData : {...this.state.reportData,sec:e.target.value}})        
    }
    onChangeElecteType =async(e)=>{
        await this.setState({elect_type:e.target.value})
    }
    loadDepartment = async() =>{        
        await this.setState({
            departments :  await this.fetchDataGet('/api/department')
        });              
    }
    loadSemester = async() =>{
        await this.setState({
            departments :  await this.fetchDataGet('/api/department')
        });   
    }

    onChangeReportView =async(e) => {
        if(e.target.name === 'report_view')
        {
            if(e.target.value === '0')
            {
                await this.setState({
                    reportVisibility : {
                        department : false,
                        sem : false,
                        sec : false,
                        course : false, 
                        catagery : false
                    },reportType:null
                })
            }

            else if(e.target.value === '1')
            {
                await this.setState({
                    reportVisibility : {
                        department : true,
                        sem : true,
                        sec : true,
                        course : true, 
                        catagery : false,
                        etype:true,
                        
                    },reportType:"G",
                    reportTable:false
                })
            }else if(e.target.value === '2')
            {
                await this.setState({
                    reportVisibility : {
                        department : true,
                        sem : true,
                        sec : true,
                        course : true, 
                        catagery : false,
                        etype:true
                    },reportType:"NS",
                    reportTable:false
                })
            }else if(e.target.value === '3')
            {
                await this.setState({
                    reportVisibility : {
                        department : true,
                        sem : true,
                        sec : true,
                        course : true, 
                        catagery : true,
                        etype:true
                    },reportType:"CW",
                    reportTable:false
                })
            }
            else if(e.target.value === '4')
            {
               await this.setState({
                    reportVisibility : {
                        department : true,
                        sem : true,
                        sec : false,
                        course : true, 
                        catagery : true,
                        etype:true
                    },reportType:"CS",
                    reportTable:false
                })
            }
        }
        this.loadDepartment();
    }

    OnchangeChangeCourse = (e) =>{
        if(e.target.name === "rollNumber"){
            this.setState({selectedCourseDetails:{...this.state.selectedCourseDetails,username:e.target.value}})}
        else if(e.target.name === "elect_group"){
            this.setState({selectedCourseDetails:{...this.state.selectedCourseDetails,elect_id:e.target.value}})}
        else if(e.target.name === "new_course"){
            this.setState({selectedCourseDetails:{...this.state.selectedCourseDetails,newCourse:e.target.value}})}
    }

    LoadSelectedCourses = async (e) =>{
        this.setState({selectedCheckboxes1:new Set()})
        this.setState({selectedCheckboxes2:new Set()})
        e.preventDefault();
        this.setState({isLoading:true});
        const responce = await this.fetchData('/api/getSelectedCourses',this.state.selectedCourseDetails);

        if(responce[0].elect_id === 2) {
            this.setState({selectedCourses: {...this.state.selectedCourses,open:responce}});
        }
        else
            this.setState({selectedCourses:{...this.state.selectedCourses,open:responce}})

            
        this.setState({isLoading:false});
    }
    enableLoading = (status)=>{
        console.log(this.state);
        if(status)
        {
            this.setState({
                isLoading:true
            })
        }else{
            this.setState({
                isLoading:false
            })
        }
    }
    async fetchData(url,data){
        try {
            this.setState({isLoading:true});
            const response = await fetch(`${project}`+url, {
                headers: {"content-type": "application/json"},
                method: 'POST',
                body: JSON.stringify(data),
                credentials: 'include'
            });
            if (response.status === 200) {
                let d = await response.json();
                console.log(url,d);
                return d;
            } else{
                let datas = await response.json();
                return {};
            }
        } catch (e) {
            console.log(e)
        } finally {
            this.setState({isLoading:false});
        }
    }
    async fetchDataGet(url){


        try {
            this.setState({isLoading:true});
            const response = await fetch(`${project}`+url, {
                headers: {"content-type": "application/json"},
                method: 'GET',
                credentials: 'include'
            });

            if (response.status === 200) {
                return await response.json();
            } else{
                return {};
            }
        } catch (e) {
            console.log(e)
        }finally {

            this.setState({isLoading:false});
        }
    }
    checkBoxInputChange =async (e)=>{

        if (e.target.name === 'cheackBoxSelectedCourse')
        {
            if(this.state.selectedCheckboxes1.has(e.target.value))
            {
                this.setState({selectedCheckboxes1:new Set()})

            }else{
                this.setState({selectedCheckboxes1:this.state.selectedCheckboxes1.add(e.target.value)})
            }
            console.log(Array.from(this.state.selectedCheckboxes1)[0]);
        }

    }
    loadAlternateCourses = async  (e)=>{
            const response = await this.fetchData('/api/getAvailableCourses',{username:this.state.selectedCourseDetails.username,choose_id:Array.from(this.state.selectedCheckboxes1)[0]});
        this.setState({availableForUpdateCourse:response});
        console.log({username:this.state.selectedCourseDetails.username,choose_id:Array.from(this.state.selectedCheckboxes1)[0]});
    }

    loadStatusCourse = async ()=>{
        const response = await  this.fetchDataGet('/api/CourseStatusAll');
        this.setState({courseStatus:response})
    }



    changeCourse= async (e)=>{
        e.preventDefault();

        const response = await  this.fetchData('/api/changeCourse',{choose_id: Array.from(this.state.selectedCheckboxes1)[0],course_id:this.state.selectedCourseDetails.newCourse });
    }
    render()
    {
        return(
            <MainContext.Provider value={{
                state:this.state,
                enableLoading:this.enableLoading,
                OnchangeChangeCourse:this.OnchangeChangeCourse,
                LoadSelectedCourses:this.LoadSelectedCourses,
                checkBoxInputChange:this.checkBoxInputChange,
                loadAlternateCourses:this.loadAlternateCourses,
                changeCourse:this.changeCourse,
                loadStatusCourse:this.loadStatusCourse,
                onChangeReportView:this.onChangeReportView,
                loadDepartment:this.loadDepartment,
                onChangeDepartment:this.onChangeDepartment,
                onChangeSemester:this.onChangeSemester,
                onChangeSection:this.onChangeSection,
                onChangeType:this.onChangeType,
                onChangeSlot:this.onChangeSlot,
                onLoadReportData:this.onLoadReportData,
                onChangeCourse : this.onChangeCourse
            }}>
                {this.props.children}
            </MainContext.Provider>
        )
    }
}
const Consumer = MainContext.Consumer;
export  {Context,Consumer};